// main package
package main

import (
	"bitbucket.org/alexmcroberts/go-lambda-go/name"
	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	lambda.Start(name.Name)
}
